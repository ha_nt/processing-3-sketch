float [] xp = new float[0]; //used to store the allocated elements
float [] yp = new float[0];
float [] diameter = new float[0];
float strokeWeight = 2;
int numObjects = 0; //used to count the number of allocated elements
void setup() {
  frameRate(120);
  size(409, 409);
  strokeWeight(strokeWeight);
}

void draw() {
  int k = 0;
  while (true) { //until you find a successful location (i.e. without an overlap)
    boolean overlap = false; //use it to mark overlaps
    float xrand = random(width); //produce a random possible location
    float yrand = random(height);
    float drand = random(20,70);
    //println("x=, "+xrand+", y="+yrand);
    //ellipse(xrand, yrand, 20, 20);
    //go through all the remaining elements
    for (int j=0; j<xp.length; j++) {
      //find distance
      float distance = dist(xrand, yrand, xp[j], yp[j]);

      //if too short then it will overlap
      if (distance < diameter[j]/2+drand/2+strokeWeight) overlap = true;
    }

    if (overlap==false) { //if no overlap then this is a successful location
      xp = append(xp, xrand); //add it to memory
      yp = append(yp, yrand);
      diameter = append(diameter, drand);
      break;
    }

    k++;
    if (k>500) { // will exit if after 500 attempts no space is found
      //println(xp.length + " impass"); //warn the user
      break;
    }
  }

  background(255);
  for (int i=0; i<xp.length; i++) //draw anything that has been allocated
    ellipse(xp[i], yp[i], diameter[i], diameter[i]);
}

void drawCircle(float x, float y, float diameter) {
  ellipse(x, y, diameter, diameter);
}
