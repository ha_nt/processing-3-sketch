float [] xp = new float[0]; //used to store the allocated elements
float [] yp = new float[0];
float [] diameter = new float[0];
float strokeWeight = 2;
int numObjects = 0; //used to count the number of allocated elements
void setup() {
  frameRate(24);
  size(409, 409);
  background(255);
  strokeWeight(strokeWeight);
}

void draw() {
  boolean overlap = false;
  float xrand = random(width);
  float yrand = random(height);
  //float drand = random(20,70);
  float drand = 50;
  //println("x=, "+xrand+", y="+yrand);
  if (mousePressed) {
    background(255);
  }
  ellipse(xrand, yrand, drand, drand);
}

void drawCircle(float x, float y, float diameter) {
  ellipse(x, y, diameter, diameter);
}
