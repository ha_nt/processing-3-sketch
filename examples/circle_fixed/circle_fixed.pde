float [] xp = new float[0]; //used to store the allocated elements
float [] yp = new float[0];
float diameter = 20;
int numObjects = 0; //used to count the number of allocated elements
void setup() {
  frameRate(120);
  size(409, 409);
}

void draw() {
  int k = 0;
  while (true) { //until you find a successful location (i.e. without an overlap)
    boolean overlap = false; //use it to mark overlaps
    float xrand = random(width); //produce a random possible location
    float yrand = random(height);
    println("x=, "+xrand+", y="+yrand);
    //ellipse(xrand, yrand, 20, 20);
    //go through all the remaining elements
    for (int j=0; j<xp.length; j++) {
      //find distance
      float distance = dist(xrand, yrand, xp[j], yp[j]);

      //if too short then it will overlap
      if (distance < diameter) overlap = true;
    }

    if (overlap==false) { //if no overlap then this is a successful location
      xp = append(xp, xrand); //add it to memory
      yp = append(yp, yrand);
      break;
    }

    k++;
    if (k>500) { // will exit if after 500 attempts no space is found
      println(xp.length + " impass"); //warn the user
      break;
    }
  }

  background(255);
  for (int i=0; i<xp.length; i++) //draw anything that has been allocated
    ellipse(xp[i], yp[i], diameter, diameter);
}

void drawCircle(int x, int y) {
  
  ellipse(x, y, diameter, diameter);
}
