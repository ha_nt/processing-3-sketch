float [] xp = new float[0]; //used to store the allocated elements
float [] yp = new float[0];
float [] diameter = new float[0];
float strokeWeight = 2;
int numObjects = 0; //used to count the number of allocated elements
void setup() {
  frameRate(24);
  size(409, 409);
  strokeWeight(strokeWeight);
}

void draw() {
  boolean overlap = false;
  float xrand = random(width);
  float yrand = random(height);
  //float drand = random(20, 70);
  float drand = 50;
  //println("x=, "+xrand+", y="+yrand);
  //ellipse(xrand, yrand, drand, drand);

  xp = append(xp, xrand); //add it to memory
  yp = append(yp, yrand);
  diameter = append(diameter, drand);

  for (int i=xp.length-1; i>=0; i--) {
    ellipse(xp[i], yp[i], diameter[i], diameter[i]);
  }
  if (mousePressed) {
    background(255);
  }
}

void drawCircle(float x, float y, float diameter) {
  ellipse(x, y, diameter, diameter);
}
